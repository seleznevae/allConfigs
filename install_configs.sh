#!/bin/bash

cp tmux/.tmux.conf ~/.tmux.conf
cp git/.gitconfig ~/.gitconfig
cp hg/.hgrc ~/.hgrc
cp bash/.my_bashrc ~/.my_bashrc
echo '##################' >> ~/.bashrc
echo '# Loading my configs' >> ~/.bashrc
echo '. ~/.my_bashrc' >> ~/.bashrc
